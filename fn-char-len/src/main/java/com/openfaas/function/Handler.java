package com.openfaas.function;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openfaas.function.bean.CharLenReqDto;
import com.openfaas.function.bean.CharLenRespDto;
import com.openfaas.model.IResponse;
import com.openfaas.model.IRequest;
import com.openfaas.model.Response;

public class Handler extends com.openfaas.model.AbstractHandler {

    public IResponse Handle(IRequest req) {
        String reqBody = req.getBody();

        ObjectMapper objectMapper = new ObjectMapper();

        CharLenReqDto charLenDto = null;
        try {
            objectMapper.readValue(reqBody, CharLenReqDto.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        CharLenRespDto respDto = new CharLenRespDto();
        if (charLenDto != null) {
            respDto.setContent(charLenDto.getContent());
            respDto.setLength(charLenDto.getContent().length());
        }

        String respBody = null;
        try {
            respBody = objectMapper.writeValueAsString(respDto);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        Response res = new Response();
        res.setBody(respBody);

        return res;
    }
}
