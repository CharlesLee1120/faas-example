package com.openfaas.function.bean;

public class CharLenReqDto {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
