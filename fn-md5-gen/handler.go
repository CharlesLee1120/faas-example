package function

import (
	"crypto/md5"
	"encoding/hex"
	handler "github.com/openfaas/templates-sdk/go-http"
	"github.com/rs/zerolog/log"
	"net/http"
)

// Handle a function invocation
func Handle(req handler.Request) (handler.Response, error) {

	var err error

	// 1. 验证输入参数
	if len(req.Body) < 1 {
		log.Info().Str("Input", string(req.Body)).Msg("输入参数无效")
		return handler.Response{
			Body:       []byte(""),
			StatusCode: http.StatusBadRequest,
		}, err
	} else {
		log.Info().Str("Input", string(req.Body)).Msg("输入参数")
	}

	// 2. MD5处理
	hash := md5.New()
	hash.Write(req.Body)

	md5Result := hex.EncodeToString(hash.Sum(nil))
	log.Info().Str("Input", string(req.Body)).Str("Output", md5Result).Msg("MD5 Generate")

	return handler.Response{
		Body:       []byte(md5Result),
		StatusCode: http.StatusOK,
	}, err
}
