module handler/function

go 1.17

require (
	github.com/openfaas/templates-sdk/go-http v0.0.0-20220408082716-5981c545cb03
	github.com/rs/zerolog v1.27.0
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)
