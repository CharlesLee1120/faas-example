package function

import (
	handler "github.com/openfaas/templates-sdk/go-http"
	"testing"
)

// Handle a serverless request
func TestHandleChineseArray(t *testing.T) {

	var req handler.Request
	req.Body = []byte("你好")

	Handle(req)
}

func TestHandleCharArray(t *testing.T) {

	var req handler.Request
	req.Body = []byte("Hello")
	Handle(req)
}

func TestHandleByteArray(t *testing.T) {

	var req handler.Request
	req.Body = []byte{102, 97, 108, 99, 111, 110}
	Handle(req)
}
